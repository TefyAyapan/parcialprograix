﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parcial2021F
{
    public class AlumnoModel
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public int? Carne { get; set; }
    }
}
