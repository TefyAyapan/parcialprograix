﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Parcial2021F.Services
{
    public class AlumnoServices
    {
        private SqlConnection _Conn = new SqlConnection();
        //Driver
        //BD
        //ORM --> Dapper (Micro ORM)
        public DatosCompletosModel GetAlumnosById(int id)
        {
            _Conn = GetSqlConnection();
            _Conn.Open();
            var alumnos = _Conn.Query<DatosCompletosModel>("Select al.Id_alumno, al.Nombre, al.Carnet, c.Nombre_curso from Alumnos al Inner join Asignaciones a on al.Id_alumno = a.Id_alumno Inner join Cursos c on c.Id_curso = a.Id_curso").Where(f => f.id_alumno == id).ToList();
            return alumnos.Count != 0 ? alumnos.First() : null;
        }
        public IEnumerable<DatosCompletosModel> GetAlumnos()
        {
            _Conn = GetSqlConnection();
            _Conn.Open();
            var alumnos = _Conn.Query<DatosCompletosModel>("Select al.Id_alumno, al.Nombre, al.Carnet, c.Nombre_curso from Alumnos al Inner join Asignaciones a on al.Id_alumno = a.Id_alumno Inner join Cursos c on c.Id_curso = a.Id_curso").ToList();
            return alumnos;
        }

        private static SqlConnection GetSqlConnection()
        {
            return new SqlConnection(@"Data Source=DESKTOP-HDK25BP\SQLEXPRESS;Initial Catalog=Parcial1;Integrated Security=True;Pooling=False");
        }
    }
}
