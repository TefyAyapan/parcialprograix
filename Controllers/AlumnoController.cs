﻿using Microsoft.AspNetCore.Mvc;
using Parcial2021F.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parcial2021F.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AlumnoController : ControllerBase
    {
      
        [HttpGet]
        public ActionResult<IEnumerable<AlumnoModel>> Get()
        {
            var alumnoService = new AlumnoServices();
            var alumno = alumnoService.GetAlumnos();
            if (alumno.Any())
            {
                return Ok(alumno);
            }
            return NotFound("Messaje: There is not clients.");
        }

        
        [HttpGet("id")]
        public ActionResult<IEnumerable<AlumnoModel>> Get(int id)
        {
            var alumnoService = new AlumnoServices();
            var alumno = alumnoService.GetAlumnosById(id);
           
                return Ok(alumno);
         
        }

    }
}
